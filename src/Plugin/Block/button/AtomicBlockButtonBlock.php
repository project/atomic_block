<?php

namespace Drupal\atomic_block\Plugin\Block\button;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Uuid\Uuid;

/**
 * Provides a 'atomic_block_button' Block.
 *
 * @Block(
 *   id = "atomic_block_button",
 *   admin_label = @Translation("Button"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockButtonBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['text'] = '';
    $default_configuration['link'] = NULL;
    $default_configuration['element_properties'] = [
      'button_background_color' => [
        'color' => '#007bff',
        'opacity' => 1,
      ],
      'button_background_color_rgba' => '',
    ];
    return $default_configuration;
  }

  /**
   * @param $form
   * @param FormStateInterface $form_state
   *
   * @return array|void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['text'],
    ];
    $form['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link url'),
      '#default_value' => $this->configuration['link'],
    ];

    // Properties Group
    $form['element_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Element properties'),
    ];

    $form['element_properties']['button_background_color'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('button ackground Color'),
    ];

    $form['element_properties']['button_background_color']['color'] = [
      '#type' => 'color',
      '#title' => $this
        ->t('Background Color'),
      '#default_value' => $this->configuration['element_properties']['button_background_color']['color'],
    ];

    $form['element_properties']['button_background_color']['opacity'] = [
      '#title' => $this->t('Button background color Opacity'),
      '#type' => 'number',
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_properties']['button_background_color']['opacity'],
    ];

    return $form;

  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['text'] = $values['text'];
    $this->configuration['link'] = $values['link'];
    $this->configuration['element_properties'] = $values['element_properties'];
    $this->configuration['element_properties']['button_background_color_rgba'] = $this->hexToRgba($values['element_properties']['button_background_color']['color'], $values['element_properties']['button_background_color']['opacity']);
    parent::blockSubmit($form, $form_state);
  }

}
