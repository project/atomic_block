<?php

namespace Drupal\atomic_block\Plugin\Block\title;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'atomic_block_title' Block.
 *
 * @Block(
 *   id = "atomic_block_title",
 *   admin_label = @Translation("Title"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockTitleBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['title'] = '';
    $default_configuration['link'] = NULL;
    $default_configuration['element_properties'] = [
      'title_type' => 'h2',
    ];
    return $default_configuration;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content: Title'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['title'],
    ];
    $form['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link url'),
      '#default_value' => $this->configuration['link'],
    ];

    // Properties Group
    $form['element_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Element Properties'),
    ];

    $form['element_properties']['title_type'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Title Type'),
      '#required' => TRUE,
      '#options' => [
        'h1' => 'h1',
        'h2' => 'h2',
        'h3' => 'h3',
        'h4' => 'h4',
        'h5' => 'h5',
        'h6' => 'h6',
      ],
      '#default_value' => $this->configuration['element_properties']['title_type'],
    ];

    return $form;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['title'] = $values['title'];
    $this->configuration['link'] = $values['link'];
    $this->configuration['element_properties'] = $values['element_properties'];

    parent::blockSubmit($form, $form_state);
  }

}
