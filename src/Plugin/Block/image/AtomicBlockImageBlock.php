<?php

namespace Drupal\atomic_block\Plugin\Block\image;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'atomic_block_image' Block.
 *
 * @Block(
 *   id = "atomic_block_image",
 *   admin_label = @Translation("Image"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockImageBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['image'] = [];
    $default_configuration['image_style'] = 'none';
    $default_configuration['content'] = [
      'display_content' => FALSE,
      'text' => '',
      'title' => '',
      'link' => NULL,
    ];
    $default_configuration['element_properties'] = [
      'display_content' => FALSE,
      'background_color_type' => 'none',
      'content_position' => [
        'top' => 0,
        'bottom' => 0,
        'left' => 0,
        'right' => 0,
      ]
    ];
    return $default_configuration;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => '200M',
      ],
      '#upload_location' => 'public://atomic_block_image/' . date("Y-m-d"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['image'],
    ];

    $styles = ImageStyle::loadMultiple();
    $image_style_name = [
      'none' => 'None',
    ];
    foreach (array_keys($styles) as $v) {
      $image_style_name["$v"] = $v;
    }

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => $image_style_name,
      '#default_value' => $this->configuration['image_style'],
    ];

    $form['content'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this
        ->t('Content'),
    ];

    $form['content']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['content']['title'],
    ];

    $form['content']['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#default_value' => $this->configuration['content']['text'],
    ];

    $form['content']['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link url'),
      '#default_value' => $this->configuration['content']['link'],
    ];

    // Properties Group
    $form['element_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Element Properties'),
    ];

    // Properties Group
    $form['element_properties']['display_content'] = [
      '#type' => 'checkbox',
      '#title' => t('Display content'),
      '#default_value' => $this->configuration['element_properties']['display_content'],
    ];

    // Properties Group
    $form['element_properties']['background_color_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Background color type'),
      '#options' => [
        'none' => $this->t('-None-'),
        'single_color' => $this->t('Single color'),
        'gradient' => $this->t('Gradient'),
      ],
      '#default_value' => $this->configuration['element_properties']['background_color_type'],
    ];

    $form['element_properties']['content_position'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Content Position'),
    ];

    $form['element_properties']['content_position']['top'] = [
      '#type' => 'number',
      '#title' => $this->t('Top (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_properties']['content_position']['top'],
    ];

    $form['element_properties']['content_position']['bottom'] = [
      '#type' => 'number',
      '#title' => $this->t('Bottom (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_properties']['content_position']['bottom'],
    ];

    $form['element_properties']['content_position']['left'] = [
      '#type' => 'number',
      '#title' => $this->t('Left (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_properties']['content_position']['left'],
    ];

    $form['element_properties']['content_position']['right'] = [
      '#type' => 'number',
      '#title' => $this->t('Right (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_properties']['content_position']['right'],
    ];


    return $form;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['image'] = $values['image'];
    $this->configuration['image_style'] = $values['image_style'];
    $this->configuration['content'] = $values['content'];
    $this->configuration['element_properties'] = $values['element_properties'];
    parent::blockSubmit($form, $form_state);
  }

  public function build() {
    $build = parent::build();
    if (!empty($this->configuration['image'])) {
      $imageFileId = implode($this->configuration['image']);
      $image = File::load($imageFileId);
      if ($image != NULL) {
        $image->setPermanent();
        $image->save();
        if ($this->configuration['image_style'] == 'none') {
          $build['#image'] = [
            '#theme' => 'image',
            '#uri' => $image->getFileUri(),
          ];
        }
        else {
          $build['#image'] = [
            '#theme' => 'image_style',
            '#style_name' => $this->configuration['image_style'],
            '#uri' => $image->getFileUri(),
          ];
        }
        $build['#image']['#attributes']['style'][] = 'width:100%;';
        $build['#image']['#attributes']['style'][] = 'height:auto;';
      }
    }
    return $build;
  }

}
