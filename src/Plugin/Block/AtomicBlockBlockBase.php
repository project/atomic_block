<?php

namespace Drupal\atomic_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Mexitek\PHPColors\Color;

/**
 * Defines ABTBlockBase.
 */
abstract class AtomicBlockBlockBase extends BlockBase {

  protected $language;

  protected $base_url;

  protected $base_path;

  protected $module_path;

  protected $block_path;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    global $base_path;
    $this->base_path = $base_path;
    $this->base_url = \Drupal::request()->getSchemeAndHttpHost();
    $this->module_path = drupal_get_path('module', 'atomic_block');
    $this->block_path = $this->getBlockPath();
  }

  public function defaultConfiguration() {
    return [
      'uuid' => NULL,
      'general_properties' => [
        'hover_display' => FALSE,
        'font_size' => NULL,
        'text_indent' => NULL,
        'is_text_bold' => FALSE,
        'text_color' => [
          'color' => '#000000',
          'opacity' => 1,
        ],
        'text_color_rgba' => '',
        'background_color' => [
          'color' => '#ffffff',
          'opacity' => 0,
        ],
        'background_color_rgba' => '',
        'text_align' => 'inherit',
        'vertical_align' => 'none',
        'min_height' => 0,
        'class' => '',
        'style' => '',
        'padding' => [
          'left' => NULL,
          'right' => NULL,
          'top' => NULL,
          'bottom' => NULL,
        ],
      ],
    ];
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Properties Group
    $form['general_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('General Properties'),
      '#weight' => 200,
    ];
    $form['general_properties']['hover_display'] = [
      '#type' => 'checkbox',
      '#title' => t('Hover Display'),
      '#default_value' => $this->configuration['general_properties']['hover_display'],
    ];

    $form['general_properties']['text_align'] = [
      '#type' => 'select',
      '#title' => $this->t('Text Align'),
      '#options' => [
        'inherit' => $this->t('inherit'),
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['general_properties']['text_align'],
    ];

    $form['general_properties']['vertical_align'] = [
      '#type' => 'select',
      '#title' => $this->t('Vertical Align'),
      '#options' => [
        'none' => $this->t('None'),
        'vertical-center' => $this->t('Center'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['general_properties']['vertical_align'],
    ];

    $form['general_properties']['font_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Font size (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['font_size'],
    ];

    $form['general_properties']['text_indent'] = [
      '#type' => 'number',
      '#title' => $this->t('Text indent (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['text_indent'],
    ];

    $form['general_properties']['is_text_bold'] = [
      '#type' => 'checkbox',
      '#title' => t('Is text bold?'),
      '#default_value' => $this->configuration['general_properties']['is_text_bold'],
    ];

    $form['general_properties']['text_color'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Text Color'),
    ];

    $form['general_properties']['text_color']['color'] = [
      '#type' => 'color',
      '#required' => TRUE,
      '#title' => $this
        ->t('Text Color'),
      '#default_value' => $this->configuration['general_properties']['text_color']['color'],
    ];

    $form['general_properties']['text_color']['opacity'] = [
      '#title' => $this->t('Text Opacity'),
      '#type' => 'number',
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['general_properties']['text_color']['opacity'],
    ];

    $form['general_properties']['background_color'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Background Color'),
    ];

    $form['general_properties']['background_color']['color'] = [
      '#type' => 'color',
      '#title' => $this
        ->t('Background Color'),
      '#default_value' => $this->configuration['general_properties']['background_color']['color'],
    ];

    $form['general_properties']['background_color']['opacity'] = [
      '#title' => $this->t('Background color Opacity'),
      '#type' => 'number',
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['general_properties']['background_color']['opacity'],
    ];

    $form['general_properties']['min_height'] = [
      '#title' => $this->t('Min height(rem)'),
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#step' => 0.01,
      '#required' => TRUE,
      '#default_value' => $this->configuration['general_properties']['min_height'],
    ];

    $form['general_properties']['padding'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Padding'),
    ];
    $form['general_properties']['padding']['left'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Padding left (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['padding']['left'],
    ];

    $form['general_properties']['padding']['right'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Padding right (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['padding']['right'],
    ];

    $form['general_properties']['padding']['top'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Padding top (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['padding']['top'],
    ];

    $form['general_properties']['padding']['bottom'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Padding bottom (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['general_properties']['padding']['bottom'],
    ];

    $form['general_properties']['class'] = [
      '#type' => 'textfield',
      '#title' => 'Class',
      '#default_value' => $this->configuration['general_properties']['class'],
    ];

    $form['general_properties']['style'] = [
      '#type' => 'textfield',
      '#title' => 'Style',
      '#description' => $this->t('Inline CSS styles. <em>In general, inline CSS styles should be avoided.</em>'),
      '#default_value' => $this->configuration['general_properties']['style'],
    ];

    return $form;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!isset($this->configuration['uuid'])) {
      $this->configuration['uuid'] = \Drupal::service('uuid')->generate();
    }
    $this->configuration['general_properties'] = $values['general_properties'];
    $this->configuration['general_properties']['text_color_rgba'] = $this->hexToRgba($values['general_properties']['text_color']['color'], $values['general_properties']['text_color']['opacity']);
    $this->configuration['general_properties']['background_color_rgba'] = $this->hexToRgba($values['general_properties']['background_color']['color'], $values['general_properties']['background_color']['opacity']);
  }

  /**
   * Default Build function, you can override it.
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => $this->pluginDefinition['id'],
      '#variables' => [
        'base_path' => $this->base_path,
        'module_path' => $this->module_path,
        'block_path' => $this->block_path,
        'language' => $this->language,
        'configuration' => $this->configuration,
      ],
    ];

    $build['#attributes']['class'][] = $this->pluginDefinition['id'];
    $build['#attributes']['class'][] = 'uuid-' . $this->configuration['uuid'];
    $build['#attributes']['class'][] = $this->configuration['general_properties']['class'];
    $build['#attributes']['class'][] = $this->configuration['general_properties']['vertical_align'];
    if (!empty($this->configuration['general_properties']['hover_display'])) {
      $build['#attributes']['class'][] = 'atomic-hover-display';
    }

    $build['#attached']['library'][] = 'atomic_block/' . 'atomic_block_general';

    // Find if the library named by plugin id exist
    $library = \Drupal::service('library.discovery')
      ->getLibraryByName('atomic_block', $this->pluginDefinition['id']);
    // If the library exist, load it.
    if ($library) {
      $build['#attached']['library'][] = 'atomic_block/' . $this->pluginDefinition['id'];
    }
    return $build;
  }

  function getBlockPath() {
    $plugin_class = $this->pluginDefinition['class'];
    $path = explode('\\', $plugin_class);
    array_pop($path);
    array_shift($path);
    array_shift($path);
    $path = implode('/', $path);
    $path = $this->base_path . $this->module_path . '/src/' . $path;
    return $path;
  }

  function hexToRgba($hex, $opacity) {
    /** @var \Mexitek\PHPColors\Color $color */
    $color = new Color($hex);
    $color_rgb_array = $color->getRgb();
    return 'rgba(' . $color_rgb_array['R'] . ',' . $color_rgb_array['G'] . ',' . $color_rgb_array['B'] . ',' . $opacity . ')';
  }

}
