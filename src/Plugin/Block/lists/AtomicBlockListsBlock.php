<?php

namespace Drupal\atomic_block\Plugin\Block\lists;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'atomic_block_lists' Block.
 *
 * @Block(
 *   id = "atomic_block_lists",
 *   admin_label = @Translation("List"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockListsBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['list'] = [];
    $default_configuration['element_properties'] = [
      'list_type' => 'ul',
      'ordered_list_style' => 'decimal',
      'unordered_list_style' => 'disc',
      'list_padding_inline_start' => 40,
      'list_item_padding' => [
        'left' => NULL,
        'right' => NULL,
        'top' => NULL,
        'bottom' => NULL,
      ],
    ];
    return $default_configuration;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    // Gather the number of links in the form already.
    $num_lists = $form_state->get('num_lists');
    // We have to ensure that there is at least one name field.
    if ($num_lists === NULL) {
      $num_lists = count($this->configuration['list']);
      $form_state->set('num_lists', $num_lists);
      if (empty($num_lists)) {
        $form_state->set('num_lists', 1);
        $num_lists = 1;
      }
    }
    $form['#tree'] = TRUE;

    // Properties Group
    $form['list'] = [
      '#type' => 'fieldset',
      '#open' => TRUE,
      '#title' => $this
        ->t('List'),
      '#prefix' => '<div id="lists-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_lists; $i++) {
      $form['list'][$i] = [
        '#type' => 'fieldset',
        '#open' => TRUE,
        '#title' => '#' . $i,
      ];
      $form['list'][$i]['text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Text'),
        '#default_value' => $this->configuration['list'][$i]['text'],
      ];

      $form['list'][$i]['url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Url'),
        '#default_value' => $this->configuration['list'][$i]['url'],
      ];
    }

    $form['list']['actions'] = [
      '#type' => 'actions',
    ];

    $form['list']['actions']['add_list'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => [[$this, 'addOne']],
      '#ajax' => [
        'callback' => [$this, 'addmoreCallback'],
        'wrapper' => 'lists-fieldset-wrapper',
      ],
    ];

    // If there is more than one name, add the remove button.
    if ($num_lists > 1) {
      $form['list']['actions']['remove_list'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => [[$this, 'removeCallback']],
        '#ajax' => [
          'callback' => [$this, 'addmoreCallback'],
          'wrapper' => 'lists-fieldset-wrapper',
        ],
      ];
    }

    // Properties Group
    $form['element_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Element Properties'),
    ];
    $form['element_properties']['list_type'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('List Type'),
      '#required' => TRUE,
      '#options' => [
        'ul' => 'Unordered',
        'ol' => 'Ordered',
      ],
      '#default_value' => $this->configuration['element_properties']['list_type'],
    ];

    $form['element_properties']['unordered_list_style'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Unordered list style'),
      '#required' => TRUE,
      '#options' => [
        'disc' => 'disc ●',
        'circle' => 'circle ο',
        'square' => 'square ■',
        'none' => 'none',
      ],
      '#default_value' => $this->configuration['element_properties']['unordered_list_style'],
    ];

    $form['element_properties']['ordered_list_style'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Ordered list style'),
      '#required' => TRUE,
      '#options' => [
        'none' => 'None',
        'decimal' => 'Decimal',
        'decimal-leading-zero' => 'Decimal leading zero 01 02 03',
        'lower-alpha' => 'Lower Alpha',
        'upper-alpha' => 'Upper Alpha',
        'lower-roman' => 'Lower Roman',
        'upper-roman' => 'Upper Roman',
      ],
      '#default_value' => $this->configuration['element_properties']['ordered_list_style'],
    ];

    $form['element_properties']['list_padding_inline_start'] = [
      '#type' => 'number',
      '#title' => $this->t('List padding inline start(px)'),
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#default_value' => $this->configuration['element_properties']['list_padding_inline_start'],
    ];

    $form['element_properties']['list_item_padding'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('List Item Padding'),
    ];
    $form['element_properties']['list_item_padding']['left'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Item padding left (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['element_properties']['list_item_padding']['left'],
    ];

    $form['element_properties']['list_item_padding']['right'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Item padding right (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['element_properties']['list_item_padding']['right'],
    ];

    $form['element_properties']['list_item_padding']['top'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Item padding top (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['element_properties']['list_item_padding']['top'],
    ];

    $form['element_properties']['list_item_padding']['bottom'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Item padding bottom (rem)'),
      '#min' => 0,
      '#max' => 20,
      '#step' => 0.01,
      '#default_value' => $this->configuration['element_properties']['list_item_padding']['bottom'],
    ];
    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the lists in it.
   */
  public static function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['list'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public static function addOne(array &$form, FormStateInterface $form_state) {
    $list_field = $form_state->get('num_lists');
    $add_button = $list_field + 1;
    $form_state->set('num_lists', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public static function removeCallback(array &$form, FormStateInterface $form_state) {
    $list_field = $form_state->get('num_lists');
    if ($list_field > 1) {
      $remove_button = $list_field - 1;
      $form_state->set('num_lists', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($values['list']['actions'])) {
      unset($values['list']['actions']);
    }
    foreach ($values['list'] as $key => $item) {
      if (empty($item['text'])) {
        unset($values['list'][$key]);
      }
    }

    $this->configuration['list'] = array_values($values['list']);
    $this->configuration['element_properties'] = $values['element_properties'];

    parent::blockSubmit($form, $form_state);
  }


}
