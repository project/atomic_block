<?php

namespace Drupal\atomic_block\Plugin\Block\text;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'atomic_block_text' Block.
 *
 * @Block(
 *   id = "atomic_block_text",
 *   admin_label = @Translation("Text"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockTextBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['text'] = '';
    return $default_configuration;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['text'],
    ];

    return $form;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['text'] = $values['text'];

    parent::blockSubmit($form, $form_state);
  }

}
