<?php

namespace Drupal\atomic_block\Plugin\Block\background_image;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'atomic_block_background_image' Block.
 *
 * @Block(
 *   id = "atomic_block_background_image",
 *   admin_label = @Translation("Backgound Image"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockBackgroundImageBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['image'] = [];
    $default_configuration['element_properties']['image_style'] = 'none';
    return $default_configuration;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => '200M',
      ],
      '#upload_location' => 'public://atomic_block_backgound_image/' . date("Y-m-d"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['image'],
    ];

    $styles = ImageStyle::loadMultiple();
    $image_style_name = [
      'none' => 'None',
    ];
    foreach (array_keys($styles) as $v) {
      $image_style_name["$v"] = $v;
    }

    // Properties Group
    $form['element_properties'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this
        ->t('Element Properties'),
    ];


    $form['element_properties']['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => $image_style_name,
      '#default_value' => $this->configuration['element_properties']['image_style'],
    ];
    return $form;
  }

  /**
   * Undocumented function.
   *
   * @param [type] $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['image'] = $values['image'];
    $this->configuration['element_properties'] = $values['element_properties'];
    parent::blockSubmit($form, $form_state);
  }

  public function build() {
    $build = parent::build();
    if (!empty($this->configuration['image'])) {
      $imageFileId = implode($this->configuration['image']);
      $image = File::load($imageFileId);
      if ($image != NULL) {
        $image->setPermanent();
        $image->save();
        if ($this->configuration['element_properties']['image_style'] == 'none') {
          $build['#image_url'] = Url::fromUri(file_create_url($image->getFileUri()));
        }
        else {
          $build['#image_url'] = ImageStyle::load($this->configuration['element_properties']['image_style'])->buildUrl($image->getFileUri());
        }
      }
    }
    return $build;
  }

}
