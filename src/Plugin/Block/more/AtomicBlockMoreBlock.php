<?php

namespace Drupal\atomic_block\Plugin\Block\more;

use Drupal\atomic_block\Plugin\Block\AtomicBlockBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Uuid\Uuid;

/**
 * Provides a 'atomic_block_more' Block.
 *
 * @Block(
 *   id = "atomic_block_more",
 *   admin_label = @Translation("More"),
 *   category = @Translation("Atomic Block"),
 * )
 */
class AtomicBlockMoreBlock extends AtomicBlockBlockBase {

  /**
   * Undocumented function.
   *
   * @return void
   */
  public function defaultConfiguration() {
    return [
      'text' => 'more',
      'uuid' => NULL,
      'link' => NULL,
    ];
  }

  /**
   * @param $form
   * @param FormStateInterface $form_state
   *
   * @return array|void
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('More Text'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['text'],
    ];
    $form['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('More Link'),
      '#default_value' => $this->configuration['link'],
    ];

    return $form;

  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!isset($this->configuration['uuid'])) {
      $this->configuration['uuid'] = \Drupal::service('uuid')->generate();
    }
    $this->configuration['text'] = $values['text'];
    $this->configuration['link'] = $values['link'];
  }
}
